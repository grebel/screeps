/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('spawner'); // -> 'a thing'
 */
 module.exports = function() {
     //var i = 0;
     var workers = ["worker",   "hauler",   "worker", "kiter",   "hauler",  "paladin",
                    "tower",   "hauler",  "worker",   "kiter", "worker", "hauler", "worker", "hauler",  "paladin",    "kiter", "worker",
                    "hauler",   "paladin",  "healer",   "hauler" ,  "kiter",    "fighter",
                    "paladin",  "tower", "hauler",    "healer", "supertower", "supertower", "supertower",
                    "supertower", "supertower", "healer", "healer", "supertower", "supertower", "supertower", "supertower",
                    "supertower", "supertower", "supertower", "supertower", "supertower", "supertower",
                    "supertower", "supertower", "supertower", "supertower", "supertower", "supertower",
                                        "supertower", "supertower", "supertower", "supertower", "supertower", "supertower",
                    "supertower", "supertower", "supertower", "supertower", "supertower", "supertower",
                    "supertower", "supertower", "supertower", "supertower", "supertower", "supertower",
                                    "supertower", "supertower", "supertower", "supertower", "supertower", "supertower",
                    "supertower", "supertower", "supertower", "supertower", "supertower", "supertower",
                    "supertower", "supertower", "supertower", "supertower", "supertower", "supertower",
                    "supertower", "supertower"];
     var creep_template = {"worker": [Game.WORK ,Game.WORK, Game.WORK, Game.MOVE],
                            "hauler": [Game.MOVE, Game.CARRY, Game.MOVE, Game.CARRY, Game.MOVE],
                            "builder": [Game.WORK, Game.WORK, Game.WORK, Game.CARRY, Game.MOVE],
                            "guard": [Game.TOUGH, Game.TOUGH, Game.ATTACK, Game.ATTACK, Game.MOVE],
                            "tower": [Game.MOVE, Game.MOVE, Game.RANGED_ATTACK, Game.RANGED_ATTACK, Game.RANGED_ATTACK],
                            "kiter": [Game.RANGED_ATTACK, Game.MOVE, Game.RANGED_ATTACK, Game.MOVE],
                            "healer": [Game.MOVE, Game.HEAL, Game.HEAL, Game.MOVE],
                            "paladin": [Game.MOVE, Game.HEAL, Game.MOVE, Game.RANGED_ATTACK],
                            "fighter": [Game.ATTACK, Game.MOVE, Game.ATTACK, Game.ATTACK, Game.MOVE],
                            "supertower":  [Game.MOVE, Game.RANGED_ATTACK, Game.RANGED_ATTACK, Game.RANGED_ATTACK, Game.RANGED_ATTACK],
     };
    
    var count = 0;
    for (var i in workers) {
        if (undefined === Game.creeps[workers[i] + count]){ //if creep does not exist
            var creep = Game.spawns.home.createCreep(creep_template[workers[i]], workers[i] + count);
            if (creep !=-4 && creep != -6) {
                Memory.creeps[creep].role = workers[i];
            }
            break;
        }
        count++;
    }
};