module.exports = function (creep) {

    if(creep.memory.sourceId === undefined) 
    {
        var availableSources = Memory.sourceArray.filter(function (source) {
            return !source [1] || !source[2];
        });
        if(availableSources.length) {
            var source = availableSources[0];
            creep.memory.sourceId = source[0];
            if(!source[1]) {
                source[1] = creep.id;
            }
            else {
                source[2] = creep.id;
            }

        }
    }
    //if there is no target on this harvester.
    var source = Game.getObjectById(creep.memory.sourceId);
    if(source) {
        creep.moveTo(source);
        creep.harvest(source);
    }
};