

var harvester = require('harvester');
var builder = require('builder');
var guard = require('guard');
var healer = require("healer");
var paladin = require('paladin');
var collect = require('collector');
var kiter = require("kiter");
var spawner = require('spawner');
var kiter = require('kiter');
var paladin = require('paladin');
var counter = require('counter');

collect.assignTargets();


if (!Memory.sourceArray && Game.spawns.home) {

    var sourceArray = Game.spawns.home.room.find(Game.SOURCES);
    var orderedSourceArray = [];

    sourceArray.forEach(function (source) {
        var stepsHome = source.pos.findPathTo(Game.spawns.home, {ignoreCreeps : true}).length;
        orderedSourceArray.push([source.id, null, null, stepsHome]);
    });

    orderedSourceArray.sort( function (a, b) {
        return a[3] > b[3];
    });

    Memory.sourceArray = orderedSourceArray;
}

spawner();

counter();

for(var name in Game.creeps) {
    var creep = Game.creeps[name];

    if(creep.memory.role == 'worker') {
        harvester(creep);
    }

    if(creep.memory.role == 'builder') {
        builder(creep);
    }

    if (creep.memory.role == 'guard' || creep.memory.role == 'fighter') {
        guard(creep);
    }
    
    if (creep.memory.role == 'kiter' || creep.memory.role == 'tower' || creep.memory.role == 'supertower') {
        kiter(creep);
    }

    if (creep.memory.role == 'healer') {
        healer(creep);
    }

    if (creep.memory.role == 'paladin') {
        paladin(creep);
    }

    if (creep.memory.role == 'collector' || creep.memory.role == 'hauler') {
        collect.collect(creep);
    }
}