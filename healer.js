/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('guard'); // -> 'a thing'
 */
var fightMethods = require('fightMethods');
 module.exports = function(creep) {
    var foundHeal = fightMethods.healFriends(creep);
    
    if (!foundHeal) {
        if (fightMethods.followUnitType(creep, Game.ATTACK) === false) fightMethods.rest(creep);
    }
 };