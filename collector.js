/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('guard'); // -> 'a thing'
 */

 function  removeFromCollectorTargets(targetId) {
    var index = Memory.collectorTargets.indexOf(targetId);
    if (index > -1) Memory.collectorTargets.splice(index, 1);
 }

 function returnEnergy(creep) {
    creep.moveTo(Game.spawns.home);
    creep.transferEnergy(Game.spawns.home);
 }

function retrieveEnergy(creep, target) {
    creep.moveTo(target);
    creep.pickup(target);
}
 

 module.exports = {

    collect: function(creep) {

    //remove all targets from all dead creeps.

    //remove all targets from array if they are not in 
    //Game.spawns.home.room.find(Game.DROPPED_ENERGY))


        var memoryTargets = creep.room.find(Game.DROPPED_ENERGY, {
            filter: function(object) {
                return object.id == creep.memory.target && object.energy > 0;
            }
        });
        
        //if energy has run out, remove from targets
        if (memoryTargets[0] === undefined || memoryTargets[0].energy === 0) {
            removeFromCollectorTargets(creep.memory.target);
            memoryTargets = [];
            creep.memory.target = null;
        }

        //if a target has been found in the creeps memory, retrieve that target.
        var targetEnergy;
        if (memoryTargets.length) {
            targetEnergy = memoryTargets[0];
            retrieveEnergy(creep, targetEnergy);
        }

        //if there 
        if(targetEnergy && creep.energy < creep.energyCapacity) {
            var closestEnemy = targetEnergy.pos.findNearest(Game.HOSTILE_CREEPS);
                if (closestEnemy && closestEnemy.pos.inRangeTo(creep.pos, 4)) {
                    returnEnergy(creep);
                    return;
                }
        }

        //if the creep has no target
        if (creep.energy == creep.energyCapacity || targetEnergy === null) {
            if (targetEnergy) {
                creep.memory.target = null;
                removeFromCollectorTargets(targetEnergy.id);
            }
            returnEnergy(creep);
        }
    },

    assignTargets: function() {
        var room = Game.spawns.home.room;

        //create list of all targets of hauler creeps.
        Memory.collectorTargets = [];
        room.find(Game.MY_CREEPS, {
            filter: function(object) {
                if (object.memory.role == "hauler" && object.memory.target) {
                    Memory.collectorTargets.push(object.memory.target);
                    return true;
                }
                return false;
            }
        });

        //find all haulers with targets that have expired, and remove their targets.
        var allHaulers = room.find(Game.MY_CREEPS, {
            filter: function(object) {
                return (object.memory.role == "hauler" && object.memory.target);
            }
        });
        for (var hauler in allHaulers) {
            var a = room.find(Game.DROPPED_ENERGY, {filter: makeHaulerFilter()});
            if (a[0] && a[0].pos === undefined) {
                allHaulers[hauler].memory.target = null;
            }
        }

        //create list of dropped energy starting with amount in descending order.
        var droppedEnergy = [];
        for (var i = 0; i < room.find(Game.DROPPED_ENERGY).length;i++) {
            var largest = 0;
            var largestId = null;
            var energyTargets = room.find(Game.DROPPED_ENERGY, {filter: makeEnergyFilter()});
            droppedEnergy[droppedEnergy.length] = largestId;
        }

        var newEnergyIds = [];
        var newEnergyArray = [];
        for (var eg in droppedEnergy) {
            var highEnergy = Game.spawns.home.room.find(Game.DROPPED_ENERGY, {
                filter: matchingEnergy()
            })[0];
            if (highEnergy) {
                var energy = highEnergy.energy;
                while (energy > 100) {
                    newEnergyArray[newEnergyArray.length] = energy;
                    newEnergyIds[newEnergyIds.length] = highEnergy.id;
                    energy = energy - 100;
                }
                if (energy > 0) {
                    newEnergyArray[newEnergyArray.length] = energy;
                    newEnergyIds[newEnergyIds.length] = highEnergy.id;
                }
            }
        }

        //find the closest available hauler for each bit of energy, starting with largest first.
        for (var i = 0;i < newEnergyArray.length;i++) {
            var energyIndex = newEnergyArray.indexOf(Math.max.apply(Math, newEnergyArray));
            var energy = newEnergyIds[energyIndex];
            newEnergyIds.splice(energyIndex, 1);
            newEnergyArray.splice(energyIndex, 1);
            var highEnergy = Game.spawns.home.room.find(Game.DROPPED_ENERGY, {
                filter: largestEnergyFilter()
            })[0];
            if (highEnergy) {
                var closestHauler = highEnergy.pos.findNearest(Game.MY_CREEPS, {
                    filter: unusedHaulersFilter()
                });
                if (closestHauler) {
                    closestHauler.memory.target = highEnergy.id;
                    if (Memory.collectorTargets.indexOf(highEnergy.id) == -1) {
                        Memory.collectorTargets.push(highEnergy.id);
                    }
                }
            }
        }

        //list of filters
        function makeEnergyFilter() {
            return function(object) {
                if (Memory.collectorTargets.indexOf(object.id) == -1 && droppedEnergy.indexOf(object.id) == -1) {
                    if (object.energy > largest) {
                        largest = object.energy;
                        largestId = object.id;
                    }
                }
            };
        }

        function makeHaulerFilter() {
            return function(object) {
                    return allHaulers[hauler].memory.target == object.id;
                };
        }


        function matchingEnergy() {
            return function(object) {
                    return object.id == droppedEnergy[eg];
            };
        }

        function largestEnergyFilter() {
            return function(object) {
                    return object.id == energy;
            };
        }

        function unusedHaulersFilter() {
            return function(object) {
                        return (object.memory.role == "hauler" && !object.memory.target && object.energy != object.energyCapacity);
            };
        }

    }
};


